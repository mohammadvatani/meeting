from mongo_client import Client
import os
from datetime import datetime
import config as c

client = Client(c.database_config)


def current_meetings():
    now = datetime.now().strftime("%Y/%m/%d")

    res= client.find(c.meeting_data_collection,
        {"date": {"$gt": now}}, 
        {"_id":0},
        sort_by=("date",1)
    )
    return res

def get_ticker_meeting_info(ticker):
    now = datetime.now().strftime("%Y/%m/%d")

    res = client.find(c.meeting_info_data_collection, 
        {"ticker":ticker},
        {"_id":0},
        sort_by={'date':-1},
        limit=1
    )
    if len(res):
        data = res[0]
        data["status"] = "finished"
        return data

    res = client.find(c.meeting_data_collection, 
        {"ticker":ticker},
        {"_id":0},
    limit=1,
    sort_by=("date",-1)
    )

    if not len(res):
        return {
            "status": "no_meeting"
        }

    meeting = res[0]
    meeting["status"] = "current"
    return meeting