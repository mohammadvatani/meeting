import pymongo

class Client:
    def __init__(self, config) -> None:
        self.user = config["username"]
        self.host = config["host"]
        self.port = config["port"]
        self.db = config["database"]
        self.password = config["password"]

    def get_client(self):
        return pymongo.MongoClient(f"mongodb://{self.user}:{self.password}@{self.host}:{self.port}/")

    def get_db(self):
        client = self.get_client()
        return client[self.db]

    def get_collection(self, coll):
        database = self.get_db()
        return database[coll]

    def insert_many(self, coll, lst):
        collection = self.get_collection(coll)
        return collection.insert_many(lst)

    def find(self, coll, *arg, **kwarg):
        collection = self.get_collection(coll)
        res = collection.find(*arg)
        if "limit" in kwarg.keys():
            res = res.limit(kwarg["limit"])
        if "sort_by" in kwarg.keys():
            res = res.sort(*kwarg["sort_by"])
        return [i for i in res]
    
    def update_many(self, coll:str, query:dict, values:dict):
        collection = self.get_collection(coll)
        return collection.update(query, values)
        
    def delete_many(self, coll:str, query:dict):
        collection = self.get_collection(coll)
        x = collection.delete_many(query)
        return x.deleted_count
