import os

database_config = {
    "username":os.environ.get("DATABASE_USER"),
    "password":os.environ.get("DATABASE_PASSWORD"),
    "host":os.environ.get("DATABASE_HOST"),
    "port":os.environ.get("DATABASE_PORT"),
    "database":os.environ.get("DATABASE")
}

meeting_data_collection = "meeting_announcments"
meeting_info_data_collection = "meeting_info"
