from flask import Flask, request, jsonify 
import os
import model
import json

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))


@app.route("/meetings/", methods=["GET"])
def get_meetings():
    current_meetings = model.current_meetings()
    return jsonify(current_meetings)


@app.route("/meetings/<ticker>", methods=["GET"])
def get_meeting_info(ticker):
    meeting_info = model.get_ticker_meeting_info(ticker)
    return jsonify(meeting_info)

# Run Server
if __name__ == '__main__':
  app.run(host='0.0.0.0', port=5000)