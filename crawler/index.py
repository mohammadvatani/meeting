from model import announcement
import handler
import time
from requests import exceptions
import config as c
import logging

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

def crawl_data():
    start_date = announcement.last_announcement_checked()
    try:
        data = announcement.get_new_announcements(start_date)
    except exceptions.RequestException as e:
        logger.exception("connection timeout...")
        time.sleep(5)
        return
    if not data:
        return
    i=0
    while i < len(data):
        save_handler = handler.new_item_handler(data[i])
        if (save_handler):
            try:
                save_handler(data[i])
                time.sleep(0.5)
            except exceptions.RequestException as e:
                logger.exception("connection timeout...")
                time.sleep(5)
                continue
            except Exception as e:
                logger.exception(f"exception: {str(e)}")
        announcement.update_last_announcment(data[i])
        i+=1

if __name__ == "__main__":
    while True:
        crawl_data()
        time.sleep(15)