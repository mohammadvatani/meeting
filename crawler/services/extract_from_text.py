import re
from utils import date
from unidecode import unidecode
from bs4 import BeautifulSoup

def extract_pattern(pattern, text):
    try:
        return re.findall(pattern, text)[0]
    except:
        return False

def extract_old_fund_amount(text):
    pattern = "<span id=\"lblFromAmount\">(.*?)</span>"
    old_fund = int(extract_pattern(pattern, text).replace(',',''))
    if old_fund > 1000_000_000:
        old_fund = int(old_fund / 1_000_000)
    return old_fund

def extract_new_fund_amount(text):
    pattern = "<span id=\"lblToAmount\">(.*?)</span>"
    new_fund = int(extract_pattern(pattern, text).replace(',', ''))
    if new_fund >  1000_000_000:
        new_fund = int(new_fund / 1_000_000)
    return new_fund 

def extract_previous_capital(text):
    pattern = "<span id=\"lblPreviousCapital\">(.*?)</span>"
    previous_capital = extract_pattern(pattern, text)
    return int(previous_capital.replace(',', ''))

def extract_new_capital(text):
    pattern = "<span id=\"lblNewCapital\">(.*?)</span>"
    new_capital = extract_pattern(pattern, text)
    return int(new_capital.replace(',', ''))

def extract_raise_way(text):
    pattern = "<span id=\"lblCapitalChangeType\">(.*?)</span>"
    raise_way = extract_pattern(pattern, text)
    return raise_way


def extract_purpose(text):
    pattern = "<span id=\"lblSubject\">(.*?)</span>"
    return extract_pattern(pattern, text)

def extract_old_fund_amount_delegated(text):
    soup = BeautifulSoup(text, "html5lib")
    element = soup.find(id="lblFromAmountBoardMember").text
    old_fund = int(element.replace(",",""))
    if old_fund > 1000_000_000:
        old_fund = int(old_fund / 1_000_000)
    return old_fund

def extract_new_fund_amount_delegated(text):
    soup = BeautifulSoup(text, "html5lib")
    element = soup.find(id="lblToAmountBoardMember").text
    new_fund = int(element.replace(",",""))
    if new_fund > 1000_000_000:
        new_fund = int(new_fund / 1_000_000)
    return new_fund

def extract_raise_way_delegated(text):
    soup = BeautifulSoup(text, "html5lib")
    element = soup.find(id="lblCapitalChangeTypeBoardMember")
    return element.text

def extract_purpose_delegated(text):
    soup = BeautifulSoup(text, "html5lib")
    element = soup.find(id="lblSubjectBoardMember")
    return element.text


def extract_new_delegated_raise_fund(text, item):
    old_fund = extract_old_fund_amount_delegated(text)
    new_fund = extract_new_fund_amount_delegated(text)
    raise_way = extract_raise_way_delegated(text)
    purpose = extract_purpose_delegated(text)
    
    return {
        "ticker": item["Symbol"],
        "oldCapital": old_fund,
        "newCapital": new_fund,
        "fundRaiseWay": raise_way,
        "purpose": purpose,
        "boardProposalDate": date.to_gregorian_strftime(unidecode(item['PublishDateTime'])),
        "premitiveRight": {}    
    }

def extract_new_raise_fund(text, item):
    old_fund = extract_old_fund_amount(text)
    new_fund = extract_new_fund_amount(text)
    raise_way = extract_raise_way(text)
    purpose = extract_purpose(text)
    return {
        "ticker": item["Symbol"],
        "oldCapital": old_fund,
        "newCapital": new_fund,
        "fundRaiseWay": raise_way,
        "purpose": purpose,
        "boardProposalDate": date.to_gregorian_strftime(unidecode(item['PublishDateTime'])),
        "premitiveRight": {}    
    }
    
def extract_meeting_date(text:str):
    return extract_pattern("<span id=\"txbDate\">(.*?)</span>",text)

def extract_raisefund_amounts(text:str):
    avarde_naghdi = "<span id=\"txbCashIncoming1\">(.*?)</span>"
    sood_anbashteh = "<span id=\"txbRetaindedEarning1\">(.*?)</span>"
    andookhteh = "<span id=\"txbReserves1\">(.*?)</span>"
    tajdid_arzyabi = "<span id=\"txbRevaluationSurplus1\">(.*?)</span>"
    sarf_saham = "<span id=\"txbSarfSaham1\">(.*?)</span>"
    return {
        "مطالبات حال شده سهامداران و آورده نقدی": int(extract_pattern(avarde_naghdi, text).replace(",","")),
        "سود انباشته": int(extract_pattern(sood_anbashteh, text).replace(",","")) ,
        "سایر اندوخته ها": int(extract_pattern(andookhteh, text).replace(",","")),
        "مازاد تجدید ارزیابی دارایی ها": int(extract_pattern(tajdid_arzyabi, text).replace(",","")),
        "آورده نقدی با سلب حق تقدم از سهامداران فعلی": int(extract_pattern(sarf_saham, text).replace(",",""))
    }


def extract_start_end_dates(text:str):
    pattern_start_date = "<span id=\"txbStartDate\">(.*?)</span>"
    pattern_end_date = "<span id=\"txbEndDate\">(.*?)</span>"
    return {
        "start_date": extract_pattern(pattern_start_date, text),
        "end_date": extract_pattern(pattern_end_date, text),
    }

def extract_old_fund_from_meeting(text:str):
    soup = BeautifulSoup(text, "html5lib")
    element = soup.find(id="ucExtraAssemblyCapital1_txbLastCapital1")["value"]
    return int(element.replace(",",""))


def extract_meeting_decisions(text:str):
    raisefund_amounts = extract_raisefund_amounts(text)
    raisefund_amounts = {k:v for k,v in raisefund_amounts.items() if v != 0}
    old_capital = extract_old_fund_from_meeting(text)

    return {
        **raisefund_amounts,
        "oldCapital":old_capital
    }

def extract_board_meeting_time(text):
    soup = BeautifulSoup(text, "html5lib")
    date = soup.find(id="txbSessionDate").text
    return date
     
def extract_board_decisions_on_raisefund(text):
    soup = BeautifulSoup(text, "html5lib")
    old_fund = soup.find(id="txbLastCapital")["value"]
    old_fund = int(old_fund.replace(",",""))
    avarde_naghdi = soup.find(id="txbCashIncoming").text
    sood_anbashteh = soup.find(id="txbRetaindedEarning").text
    andookhteh = soup.find(id="txbReserves").text
    tajdid_arzyabi = soup.find(id="txbRevaluationSurplus").text
    sarf_saham = soup.find(id="txbSarfSaham").text

    dic_decision = {
        "مطالبات حال شده سهامداران و آورده نقدی": int(avarde_naghdi.replace(",","")),
        "سود انباشته": int(sood_anbashteh.replace(",","")),
        "سایر اندوخته ها": int(andookhteh.replace(",","")),
        "مازاد تجدید ارزیابی دارایی ها": int(tajdid_arzyabi.replace(",","")),
        "آورده نقدی با سلب حق تقدم از سهامداران فعلی": int(sarf_saham.replace(",","")),
    }
    dic_decision = {k:v for k,v in dic_decision.items() if v != 0}
    return {
        **dic_decision,
        "oldCapital":old_fund
    }


def extract_meeting_info(text:str):
    soup = BeautifulSoup(text, "html5lib")
    date = soup.find(id="txbDate").text
    hour = soup.find(id="txbHour").text

    return date, hour

def extract_meeting_divided_profit(text:str):
    soup = BeautifulSoup(text, "html5lib")
    devidend = soup.find(id="ucAssemblyPRetainedEarning_grdAssemblyProportionedRetainedEarning_ctl18_Span1").text
    devidend = int(unidecode(devidend.replace(',','')))
    total = soup.find(id="ucAssemblyPRetainedEarning_grdAssemblyProportionedRetainedEarning_ctl17_Span1").text
    total = int(unidecode(total.replace(',','')))
    return devidend, int(devidend/total * 100)