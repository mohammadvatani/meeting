from services.db_setup import db_setup
import config as c

db_conn = db_setup(c.database_config)
