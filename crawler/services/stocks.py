from services import db_conn
import config as c


def read_stocks():
    show = {"ticker": 1}
    return db_conn.find(c.stock_collection, show=show)
