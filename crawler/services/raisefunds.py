from services import db_conn
import config as c


def get_last_raisefund_id(ticker):
    res = db_conn.find(
        c.raisefund_collection,
        limit=1,
        query={"ticker": ticker},
        sort_by=("boardProposalDate", -1)
    )
    try:
        return res[-1]["_id"]
    except:
        pass

def get_last_raisefund(ticker):
    res = db_conn.find(
        c.raisefund_collection,
        limit=1,
        query={"ticker": ticker},
        sort_by=("boardProposalDate", -1)
    )
    try:
        return res[-1]
    except:
        pass

def get_related_raise_fund_id(query):
    res = db_conn.find(
        c.raisefund_collection,
        limit=1,
        query=query
    )
    try:
        return res[-1]["_id"]
    except:
        pass