import os
# database config

database_password = os.environ.get('DATABASE_PASSWORD')
database_user = os.environ.get('DATABASE_USER')
database_host = os.environ.get('DATABASE_HOST')
database_port = os.environ.get('DATABASE_PORT')
database = os.environ.get("DATABASE")

database_config = {
    "username": database_user,
    "password": database_password,
    "host": database_host,
    "port": database_port,
    "database": database
}

meeting_data_collection = "meeting_announcments"
meeting_info_data_collection = "meeting_info"

default_start_date = '2020/10/30 00:00:00'


CONNECTION_ERROR = 500

