from codal import keys
import codal
from model.save_raise_fund import (
    add_new_meeting_announcment,
    get_meeting_info
)


def have_key(title, keys):
    for key in keys.values():
        if key in title:
            return 1


def new_item_handler(item):
    title = item["Title"].replace("\u200c"," ")

    if (keys['meeting_announcment'] in title):
        print(item["Title"])
        return add_new_meeting_announcment

    if (keys["meeting_info"] in title) and (codal.ANNOUNCMENT not in title):
        return get_meeting_info
