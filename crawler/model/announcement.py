import sys
sys.path.append('.')
sys.path.append('./utils')
from services import db_conn
import codal
from utils import urls, date
from services import request
from unidecode import unidecode
import config as c
import time
from datetime import datetime, timedelta


def last_announcement_checked():
    last_announcment = db_conn.find(c.meeting_data_collection, query={"lastAnnouncment":{ "$regex": "(.*?)" }}, limit=1, sort_by= ('_id',1))
    if len(last_announcment):
        return last_announcment[0]["lastAnnouncment"]
    default_start_date =(datetime.now() - timedelta(days=30)).strftime("%Y/%m/%d %H:%M:%S")
    db_conn.insert_many(c.meeting_data_collection, [{"lastAnnouncment":default_start_date}])
    return default_start_date

def get_new_announcements(start_date):
    results = []
    page = 1
    while True:
        time.sleep(1)
        print("page", page)
        url = urls.url_formatter(codal.REQUSET_PAGES, page)
        res = request.get_announcement_Letters(url)
        if not res:
            return
        if not len(res):
            return results[::-1]
        for element in res:
            element_datetime = date.to_gregorian_strftime(unidecode(element['PublishDateTime'])) 
            if date.compare_date(element_datetime, start_date):
                results.append(element)
            else:
                return results[::-1]
        page += 1

def update_last_announcment(item):
    return db_conn.update_many(
        c.meeting_data_collection,
        {"lastAnnouncment":{ "$regex": "(.*?)" }},
        {"$set":{"lastAnnouncment": date.to_gregorian_strftime(unidecode(item['PublishDateTime']))}}
    )
