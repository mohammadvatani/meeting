from services import db_conn, request
import services.extract_from_text as eft
import codal
import config as c
from utils import date
from unidecode import unidecode

def add_new_meeting_announcment(item):
    source_page = request.get_target_source_page(codal.BASE_URL + item['Url'])
    meeting_date, meeting_hour = eft.extract_meeting_info(source_page)

    db_conn.insert_many(c.meeting_data_collection,[{
        "title": item["Title"],
        "date": date.to_gregorian_date(meeting_date),
        "hour": meeting_hour,
        "ticker": item["Symbol"],
        "announcment_datetime": date.to_gregorian_strftime(unidecode(item["PublishDateTime"])),
        "dayOfWeek": date.day_of_week_from_persian_datetime(meeting_date) 
    }])

def get_meeting_info(item):
    source_page = request.get_target_source_page(codal.BASE_URL + item['Url'])

    divided_profit, percentage = eft.extract_meeting_divided_profit(source_page)
    db_conn.insert_many(c.meeting_info_data_collection,[{
        "title": item["Title"],
        "devidend": divided_profit,
        "ticker": item["Symbol"],
        "announcment_datetime": date.to_gregorian_strftime(unidecode(item["PublishDateTime"])),
        "percentage_devidend": percentage
    }])